import { View } from '../core/View.class.js'
import { appSearch } from '../components/search.component.js'
import { appProductList } from '../components/productList.component.js'

const homeView = new View()

homeView.use('template', () => {
	return `
		<div id="app-search" class="teste"></div>
		<div id="app-product-list"></div>	
		<div id="app-product-list-2"></div>	
	`
})

homeView.use('components', () => {


	appSearch
		.setUid('home:appSearch')
		.register('#app-search')
		.render()

	appProductList
		.setUid('home:appProductList')
		.register('#app-product-list')
		.render()

	appProductList
		.setUid('home:appProductList2')
		.register('#app-product-list-2')
		.render()

})

export { homeView }