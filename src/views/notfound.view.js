import { View } from '../core/View.class.js'

const notFoundView = new View()

notFoundView.use('template', () => {
	return `
		<h1>404 - Você tentou acessar uma página que não existe...</h1>
	`
})

notFoundView.use('components', () => {})

export { notFoundView }