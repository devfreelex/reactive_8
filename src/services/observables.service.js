import { pubsub } from '../core/pubsub.js'

const observers = () => {
	const productSubject = pubsub()

	return {
		productSubject
	}
}

const productSubject = observers().productSubject
export { productSubject }