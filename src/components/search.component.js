import { Component } from '../core/Component.class.js'
import { productSubject } from '../services/observables.service.js'
import './search.component.scss'

const appSearch = new Component()

appSearch.use('state', {
	term:null,
	title:'Digite um termo para buscar',
	buttonTitle:'Buscar'
})

appSearch.use('template', (data) => {
	return `
		<div class="search">
			<label class="box">
				<span class="title">${data.title}</span>
				<input type="text" id="inputSearch">
				<button id="sendSearch">${data.buttonTitle}</button>
			</label>
		</div>
	`
})

appSearch.use('methods', {
	search (data) {	
		productSubject.emit('product:searchByTerm', { 
			searchTerm: data.term 
		})
	},
	setTerm (data, value) {
		data.term = value
	}
})

appSearch.use('listeners', {

	onPressButton (data, query, on, methods) {
		const button = query('#sendSearch')
		on('click', button, ({target}) => methods.search(data))
	},

	onInputType (data, query, on, methods) {
		const inputSearch = query('#inputSearch')
		on('keyup', inputSearch, ({target}) => methods.setTerm(data, target.value))
	}
	
})

export { appSearch }