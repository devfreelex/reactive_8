import { Component } from '../core/Component.class.js'
import { productSubject } from '../services/observables.service.js'
import './productList.component.scss'

const appProductList = new Component()

appProductList.use('state', {
	buttonTitle: 'Remover',
	productList: [
		{ id: '01', title: 'Produto 01' },
		{ id: '02', title: 'Produto 02' },
		{ id: '03', title: 'Produto 03' },
		{ id: '04', title: 'Produto 04' },
		{ id: '05', title: 'Produto 05' },
	]
})

appProductList.use('hooks', {

	onInit (data, methods) {
		const params = {
			buttonTitle: data.buttonTitle,
			productList: data.productList, 	
			methods	
		}
		productSubject.on('product:searchByTerm', params, methods.findByTerm)	
	}

})

appProductList.use('template', (data) => {
	return `
		<ul class="product-list">
			${data.productList.map((product) => {
				return `
					<li class="product-item" style="padding:15px;">
						<span class="product-title">${product.title}</span>
						<button class="remove-item" product-id="${product.id}">${data.buttonTitle}</button>
					</li>					
				`
			}).join('')}
		</ul>
	`
})

appProductList.use('methods', {

	findByTerm({ productList, buttonTitle, methods, payload }) {
		const product = productList.find( product => {
			const productTitle = methods.normalize(product.title)
			const term = methods.normalize(payload.searchTerm)
			if(productTitle === term) return product
		})
		methods.setNewState({ product, buttonTitle }) 
	},

	normalize (text) { 
		if(!text) return
		return text.replace(/\s+/g, '').toLowerCase()
	},

	setTerm (value, data) {
		data.term = value
	},

	setNewState (data) {
		if(!data || !data.product) {
			appProductList.render()
			return
		}

		const {buttonTitle} = data
		const newState = { productList: [data.product], buttonTitle}
		appProductList.render(newState)
	}

})

appProductList.use('listeners', {

	// onPressButton (data, query, methods) {
	// 	const button = query('#sendSearch')
	// 	button.onclick = (e) => {
	// 		methods.search(data)
	// 	}
	// },

	// onInputType (data, query, methods) {
	// 	const input = query('#dataSearch')
	// 	input.onkeyup = (e) => {
	// 		methods.setTerm(e.target.value, data)
	// 	}
	// }
	
})

export { appProductList }