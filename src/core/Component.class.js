import { componentSubject } from './pubsub.js'

class Component {
	constructor() {
		this.parent = {}
		this.state = {}
		this.methods = {}
		this.listeners = {}
		this.template = () => {}
		this.uid = null
		this.hooks = {}
		this.initHooks()
	}

	use (prop, payload) {
		this[prop] = payload
	}

	setUid (uid) {
		this.uid = uid
		return this
	}

	onInit (callback) {
		this.init = () => callback(this.methods)
	}

	register (selector) {
		this.parent = document.querySelector(selector)
		return this
	}

	findElement (selector, target) {
		return target.querySelectorAll(selector)
	}

	bindEvents(eventName, targets, callback) {
		Array.from(targets).forEach( target => {
			target[`on${eventName}`] = callback
		})

		// return callback
	}

	startEventBind (element) {
		const query = (selector) => this.findElement(selector, element)
		const on = (eventName, targets, callback) => this.bindEvents(eventName, targets, callback)

		for (let position in this.listeners) {
			this.listeners[position](this.state, query, on, this.methods)
		}
	}

	renderWithState (state) {
		this.parent.setAttribute('uid', this.uid)
		this.parent.innerHTML = this.template(state)
		this.startEventBind(this.parent)
	}

	renderWithoutState() {
		this.parent.setAttribute('uid', this.uid)
		this.parent.innerHTML = this.template(this.state)
		this.startEventBind(this.parent)
	}

	initHooks () {
		setTimeout(() => {
			if (this.hooks && this.hooks.onInit) this.hooks.onInit(this.state, this.methods)		
		}, 500)
	}

	onAfterRender () {
		// componentSubject.on(`${this.uid}:afterRender`, (payload) => {
		// 	if (payload.uid === this.uid) {
		// 		console.log('after: ', this)
		// 	}
		// })
	}

	onBeforeRender () {
		// componentSubject.on(`${this.uid}:beforeRender`, (payload) => {
		// 	if (payload.uid === this.uid) {
		// 		console.log('before: ', this)
		// 	}
		// })
	}

	render (state) {
		this.onBeforeRender()
		!state ? this.renderWithoutState() : this.renderWithState(state)
		this.onAfterRender()
	}
}

export { Component }