const pubsub = () => {
	const listeners = {}

	const hasEvent = (eventName) => {
		const keys = Object.keys(listeners)
		return keys.some(key => key === eventName)
	}

	const isEquals = (callbacks, handler) => {
		return callbacks.some( callback => {
			return callback == handler
		})
	}

	const on = (eventName, params = null, handler) => {

		if(hasEvent(eventName)) {
			if(!isEquals(listeners[eventName]['handlers'], handler)) {
				listeners[eventName]['params'] = params
				listeners[eventName]['handlers'].push(handler)
			}
			return
		}

		listeners[eventName] = {}
		listeners[eventName]['params'] = params
		listeners[eventName]['handlers'] = [handler]

		// console.log(listeners)
	}
	const off = () => {}
	const emit = (eventName, payload) => {

		const listenerKeys = Object.keys(listeners)

		const listener = listenerKeys.map( keyName => {
			if (keyName === eventName) return listeners[keyName]
		})
		
		const [subject] = listener
		subject.params.payload = payload
		// console.log(subject.params)
		subject.handlers.forEach( handler => handler(subject.params))
	}
	
	return { on, off, emit}
}

const subject = pubsub()
const componentSubject = pubsub()

export { pubsub, subject, componentSubject }