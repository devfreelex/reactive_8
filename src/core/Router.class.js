import { pubsub } from '../core/pubsub.js'

const Router = (newRoutes) => {
	const routes = newRoutes || []
	const subject = pubsub()
	let element = null

	const clearElement = () => element.innerHTML = ''

	const setElement = function (selector) {
		element = document.querySelector(selector)
		return this
	}

	const renderView = (view) => {
		if(!view) {
			clearElement()
			return
		}
		view.render(element)
		view.initComponents()
	}

	const execRoute = (route) => {
		const view = route.exec()
		renderView(view)
	}

	const redirectTo = (route) => {
		window.location.hash = route.hash
	}

	const initHooks = () => {
		subject.on('router:loadRoute', {}, ({payload: route}) => {
			redirectTo(route)
			execRoute(route)
		})	
	}

	const getInitialRoute = () => {
		return routes.find( route => {
			if(route && route.initial) return route
		})
	}

	const getRouteByHash = (currentHash) => {
		return routes.find( route => {
			if(route.hash === currentHash) return route
		})
	}

	const getDefaultRoute = () => {
		return routes.find( route => {
			if(route && route.default === true) return route
		})
	}

	const loadInitial = () => {
		const initialRoute = getInitialRoute()
		subject.emit('router:loadRoute', initialRoute)
	}

	const loadDefaultRoute = () => {
		const defaultRoute = getDefaultRoute()
		redirectTo(defaultRoute)
		execRoute(defaultRoute)
	}
	
	const init = function () {
		window.addEventListener('DOMContentLoaded', (e) => {
			initHooks()
			loadInitial()
		})

		window.onhashchange = (e) => {
			const currentHash = window.location.hash
			const route = getRouteByHash(currentHash)
			if(!route) {
				loadDefaultRoute()
				return
			}
			redirectTo(route)
			execRoute(route)
		}
	}

	const expose = { init, setElement }
	return expose
}

export { Router }