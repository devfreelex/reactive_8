class View {
	constructor() {
		this.template = () => ''
		this.components = () => ''
	}

	use (prop, value) {
		this[prop] = value
	}

	render (target) {
		target.innerHTML = this.template()
	}

	initComponents () {
		this.components()
	}
}

export { View }