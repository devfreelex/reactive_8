//POLYFILLS
import 'whatwg-fetch'
import '@babel/polyfill'

import './assets/styles/reset.css'  //DEFAULT RESET CSS
import './assets/styles/styles.css' //GLOBAL CSS STYLES

import { Router } from './core/Router.class.js'
import { homeView } from './views/home.view.js'
import { notFoundView } from './views/notfound.view.js'

const routes = [
	{ hash: '#/', initial: true, exec: () => homeView },
	{ hash: '#/404', default: true, exec: () => notFoundView },
	{ hash: '#/other', exec: () => console.log('other view') },
]

Router(routes)
	.setElement('#router-view')
	.init()


